
<!-- CRM/Pcpcard/Page/PcpCard.php -->
<div class="crm-results-block">

{if $action eq 16}
    <div id="pcpcontacttab">
        {include file="CRM/common/jsortable.tpl"}
        {strip}
            <table id="options" class="display crm-sortable" data-order='[[3,"desc"]]'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{ts}Page Title{/ts}</th>
                        <th>{ts}Contribution Page / Event{/ts}</th>
                        <th>{ts}No. Contributions{/ts}</th>
                        <th>{ts}Thermometer{/ts}</th>
                        <th>{ts}Goal Amount{/ts}</th>
                        <th>{ts}Status{/ts}</th>
                        {if $civiAdmin}<th data-orderable="false"></th>{/if}
                    </tr>
                </thead>

                {foreach from=$pages item=page}
                    <tr class="{cycle values="odd-row,even-row"}">
                        <td>{$page.pcp_id}</td>
                        <td><a href="{crmURL p='civicrm/pcp/info' q="reset=1&id=`$page.pcp_id`" fe='true'}" title="{ts}View Personal Campaign Page{/ts}" target="_blank">{$page.title}</a></td>
                        <td title="Campaign #{$page.campaign_id}. Page #{$page.page_id}. Page Type {$page.page_type}.">
                        {if $page.page_type eq 'contribute' && $civiAdmin}
                            <a target="_blank" href="{crmURL p='civicrm/admin/contribute/settings' q="reset=1&id=`$page.page_id`"}">{$page.page_title}</a>
                        {elseif $page.page_type eq 'event' && $eventAdmin}
                            <a target="_blank" href="{crmURL p='civicrm/event/manage/settings' q="reset=1&id=`$page.page_id`"}">{$page.page_title}</a>
                        {else}
                        {$page.page_title}
                        {/if}
                        </td>
                        <td>
                            {if $page.page_type == 'contribute' && $editContributionsAdmin}
                            <a target="_blank" href="{crmURL p='civicrm/contribute/search' q="reset=1&force=1&pcp_id=`$page.pcp_id`"}" title="{ts}View Contributions{/ts}">
                                {$page.number_of_contributions}
                            </a>
                            {else}
                                {$page.number_of_contributions}
                            {/if}
                        </td>
                        <td>{$page.thermometer|crmMoney}</td>
                        <td>{$page.goal_amount|crmMoney}</td>
                        <td>{$page.status}{if $page.is_active eq 0}<br><span style="opacity:0.5">Inactive</span>{/if}</td>
                        {if $civiAdmin}<td class="nowrap">{$page.actions}</td>{/if}
                    </tr>
                {/foreach}
            </table>
        {/strip}
    </div>
{/if}{* end of action eq 16 *}
</div>
{if $civiAdmin}<p>CiviAdmin Tip: Hover over the contribution/event title for technical details.</p>{/if}

{literal}
<script>
CRM.$(function($) {
  var active = 'a.button, a.action-item:not(.crm-enable-disable):not(.crm-renew-revert), a.crm-popup';
  $('#crm-main-content-wrapper')
    // Widgetize the content area
    .crmSnippet()
    // Open action links in a popup
    .off('.crmLivePage')
    .on('click.crmLivePage', active, CRM.popup)
    .on('crmPopupFormSuccess.crmLivePage', active, CRM.refreshParent);
});
</script>
{/literal}
