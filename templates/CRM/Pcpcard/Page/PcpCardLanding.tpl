
<h3>Welcome!</h3>

<p>Browse to any contact card and find the new tab labeled <em>Personal Campaign Pages</em>.</p>


<h3>Who we are & Our Mission</h3>
<p>This CiviCRM extension has been produced under the watch of Noah's Light Foundation.</p>

<p>Our Mission: To find a cure for pediatric brain cancer by supporting visionary doctors in pediatric oncology, funding new research in the field, raising awareness of our cause, and enriching the lives of the brave children struggling with this disease.</p>
<p><a href="https://www.noahslightfoundation.org" target="_blank">Noah's Light Foundation Website</a> (opens in new tab)</p>

<h4>Email us</h4>
<p>webmaster17510@noahslightfoundation.org</p>