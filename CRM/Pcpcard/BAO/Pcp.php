<?php

class CRM_Pcpcard_BAO_Pcp extends CRM_PCP_BAO_PCP {

  public static function getParentInfo($page_type = 'contribute', $page_id = 1) {

    $parent_info = array();

    if ($page_type == 'contribute') {
      try {
        $result = civicrm_api3('ContributionPage', 'get', array(
          'sequential' => 1,
          'return' => array("campaign_id","title"),
          'id' => $page_id,
        ));
        $parent_info = $result['values'][0];
      } catch (Exception $e) {
        CRM_Core_Error::debug_log_message('pcpcard ' . $e);
      }
    }

    if ($page_type == 'event') {
      try {
        $result = civicrm_api3('Event', 'get', array(
          'sequential' => 1,
          'return' => array("campaign_id","title"),
          'id' => $page_id,
        ));
        $parent_info = $result['values'][0];
        
      } catch (Exception $e) {
        CRM_Core_Error::debug_log_message('pcpcard ' . $e);
      }
    }

    if (!array_key_exists('title', $parent_info)) $parent_info['title'] = 'NA';
    if (!array_key_exists('campaign_id', $parent_info)) $parent_info['campaign_id'] = 0;
    return $parent_info;
  }

  public static function getNumberOfContributions($pcp_id) {
    $count = 0;

    try {
        $result = civicrm_api3('ContributionSoft', 'getcount', array(
          'sequential' => 1,
          'pcp_id' => $pcp_id,
        ));

        $count = $result;
    } catch (Exception $e) {
        $count = 0;
    }

    return $count;
  }

}
