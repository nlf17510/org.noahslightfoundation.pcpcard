<?php

class CRM_Pcpcard_Page_PcpCard extends CRM_Core_Page {

  public function preProcess() {
    $this->_contactId = CRM_Utils_Request::retrieve('cid', 'Positive', $this);
    $this->_action = CRM_Utils_Request::retrieve('action', 'String', $this, FALSE, 'browse');
    $this->assign('action', $this->_action);
  }

  public function run() {

  	$this->preProcess();

    if ($this->_action & CRM_Core_Action::REVERT) {
      $pcp_id = CRM_Utils_Request::retrieve('pcp_id', 'Positive', $this, FALSE);
      CRM_PCP_BAO_PCP::setIsActive($pcp_id, 0);
      $url = CRM_Utils_System::url('civicrm/contact/view',
      'action=browse&selectedChild=pcpcard&cid=' . $this->_contactId);
      CRM_Utils_System::redirect($url);
    }
    elseif ($this->_action & CRM_Core_Action::RENEW) {
      $pcp_id = CRM_Utils_Request::retrieve('pcp_id', 'Positive', $this, FALSE);
      CRM_PCP_BAO_PCP::setIsActive($pcp_id, 1);
      $url = CRM_Utils_System::url('civicrm/contact/view',
      'action=browse&selectedChild=pcpcard&cid=' . $this->_contactId
    );
      CRM_Utils_System::redirect($url);
    }
    elseif ($this->_action & CRM_Core_Action::DELETE) {
      $pcp_id = CRM_Utils_Request::retrieve('pcp_id', 'Positive', $this, FALSE);
      CRM_PCP_BAO_PCP::deleteById($pcp_id);
      $url = CRM_Utils_System::url('civicrm/contact/view',
      'action=browse&selectedChild=pcpcard&cid=' . $this->_contactId
    );
      CRM_Utils_System::redirect($url);
    }

    $status = CRM_PCP_BAO_PCP::buildOptions('status_id', 'create');

    if (CRM_Core_Permission::check('administer CiviCRM')):
      $this->assign('civiAdmin', TRUE);
    endif;

    if (CRM_Core_Permission::check('edit event participants')):
      $this->assign('eventAdmin', TRUE);
    endif;

    if (CRM_Core_Permission::check('edit contributions')):
      $this->assign('editContributionsAdmin', TRUE);
    endif;
    


    $deleteExtra = ts('Are you sure you want to delete this Campaign Page ?');
    $links = array(
        CRM_Core_Action::UPDATE => array(
            'name' => ts('Edit'),
            'url' => 'civicrm/pcp/info',
            'qs' => 'action=update&reset=1&id=%%pcp_id%%&context=dashboard',
            'title' => ts('Edit Personal Campaign Page'),
            'ref' => 'crm-popup',
        ),
        CRM_Core_Action::RENEW => array(
          'name' => ts('Approve'),
          'url' => 'civicrm/pcp-card',
          'qs' => 'action=renew&cid=%%cid%%&pcp_id=%%pcp_id%%',
          'title' => ts('Approve Personal Campaign Page'),
          'ref' => 'crm-renew-revert',
        ),
        CRM_Core_Action::REVERT => array(
          'name' => ts('Reject'),
          'url' => 'civicrm/pcp-card',
          'qs' => 'action=revert&cid=%%cid%%&pcp_id=%%pcp_id%%',
          'title' => ts('Reject Personal Campaign Page'),
          'ref' => 'crm-renew-revert',
        ),
        CRM_Core_Action::DELETE => array(
          'name' => ts('Delete'),
          'url' => 'civicrm/pcp-card',
          'qs' => 'action=delete&pcp_id=%%pcp_id%%&selectedChild=pcpcard',
          'extra' => 'onclick = "return confirm(\'' . $deleteExtra . '\');"',
          'title' => ts('Delete Personal Campaign Page'),
        ),
    );

    $contact_id = $this->_contactId;
    $pcps = civicrm_api3('Pcp', 'get', array(
      'contact_id' => $contact_id,
      'sequential' => 1
    ));
    $pages = array();
    if(!empty($pcps['values'])) {
      foreach($pcps['values'] as $pcp) {

        $mask = array_sum(array_keys($links));

        switch ($pcp['status_id']) {
          case 2:
            $mask -= CRM_Core_Action::RENEW;
            break;

          case 3:
            $mask -= CRM_Core_Action::REVERT;
            break;
        }

        $parent_info = CRM_Pcpcard_BAO_Pcp::getParentInfo($pcp['page_type'], $pcp['page_id']);

        $pages[$pcp['id']] = array(
            'pcp_id' => $pcp['id'],
            'title' => $pcp['title'],
            'status' => $status[$pcp['status_id']],
            'campaign_id' => $parent_info['campaign_id'],
            'event' => '12',
            'event_fee' => '12',
            'goal_amount' => $pcp['goal_amount'],
            'is_active' => $pcp['is_active'],
            'page_id' => $pcp['page_id'],
            'page_type' => $pcp['page_type'],
            'page_title' => $parent_info['title'],
            'number_of_contributions' => CRM_Pcpcard_BAO_Pcp::getNumberOfContributions($pcp['id']),
            'thermometer' => CRM_PCP_BAO_PCP::thermoMeter($pcp['id']),
            'actions' => CRM_Core_Action::formLink($links, $mask, array('pcp_id' => $pcp['id'], 'cid'=>$contact_id)),
            'target_type' => isset($target_entity_info['type']) ? $target_entity_info['type'] : null,
            'target_id' => isset($target_entity_info['id']) ? $target_entity_info['id'] : null
        );
      }
    }

    krsort($pages);

    $this->assign('pages', $pages);

    parent::run();
  }

}
