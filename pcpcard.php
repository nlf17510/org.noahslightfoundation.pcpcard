<?php

require_once 'pcpcard.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function pcpcard_civicrm_config(&$config) {
  _pcpcard_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function pcpcard_civicrm_xmlMenu(&$files) {
  _pcpcard_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function pcpcard_civicrm_install() {
  _pcpcard_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function pcpcard_civicrm_postInstall() {
  _pcpcard_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function pcpcard_civicrm_uninstall() {
  _pcpcard_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function pcpcard_civicrm_enable() {
  $url = CRM_Utils_System::url('civicrm/pcp-card-landing', 'reset=1');
  CRM_Core_Session::setStatus('Extension enabled with success! <a href="'.$url.'">Click here</a> to read the welcome page.', 'PCP Card', 'success', array('expires' => 0));
  _pcpcard_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function pcpcard_civicrm_disable() {
  _pcpcard_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function pcpcard_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _pcpcard_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function pcpcard_civicrm_managed(&$entities) {
  _pcpcard_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function pcpcard_civicrm_caseTypes(&$caseTypes) {
  _pcpcard_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function pcpcard_civicrm_angularModules(&$angularModules) {
  _pcpcard_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function pcpcard_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _pcpcard_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function pcpcard_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function pcpcard_civicrm_navigationMenu(&$menu) {
  _pcpcard_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'org.noahslightfoundation.pcpcard')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _pcpcard_civix_navigationMenu($menu);
} // */

function pcpcard_civicrm_tabs( &$tabs, $contactID ) {

  try {
    $count = civicrm_api3('Pcp', 'getcount', array('sequential' => 1, 'contact_id' => $contactID));
  } catch (\Exception $e) {
    $count = 0;
  }

  $url = CRM_Utils_System::url('civicrm/pcp-card','reset=1&cid='.$contactID);
  $tabs[] = array('id' => 'pcpcard', 'url' => $url, 'title' => 'Personal Campaign Pages', 'weight' => 300, 'count' => $count);
}
